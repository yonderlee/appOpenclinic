package pe.gob.sis;

import java.util.Vector;

public class Procedimientos extends SIS_Object{

	public Procedimientos() {
		super(12, "SIS_PROCEDIMIENTOS");
	}

	public static Vector getForFUA(String uid){
		return SIS_Object.getForFUA("SIS_PROCEDIMIENTOS", 12, uid);
	}
	
	public static void moveToHistory(String uid){
		moveToHistory("SIS_PROCEDIMIENTOS", 12, uid);
	}
	
	public String getErrors(){
		String errors = "";
		for(int n=1;n<=fields;n++){
			if(!isValid(n)){
				if(errors.length()>0){
					errors+=",";
				}
				errors+=n;
			}
		}
		return errors;
	}

	boolean isValid(int fieldid){
		if("*1*4*".contains("*"+fieldid+"*")){
			return getValueInt(fieldid)>0;
		}
		else if("*2*3*".contains("*"+fieldid+"*")){
			return FUA.isValidCPT(getValueString(fieldid));
		}
		else if("*8*".contains("*"+fieldid+"*")){
			return getValueString(fieldid).length()==0 || getValueInt(fieldid)==1 || getValueInt(fieldid)==3;
		}
		return true;
	}

}
